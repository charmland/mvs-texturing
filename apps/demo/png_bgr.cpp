// png_bgr.cpp.cpp
// Created by chenglong on 2018/8/31.
// Copyright © 2018 www.zhimei.ai All rights reserved.
//

#include <util/timer.h>
#include "mve/image_io.h"

int main(int argc, const char*argv[]){
util::WallTimer timer;
mve::ByteImage::Ptr image = mve::image::load_file(argv[1]);
std::cout<<"load time="<<timer.get_elapsed()<<"ms."
        <<"ch0="<<image->at(2320/2,3088/2,0)<<",ch1="<<image->at(2320/2,3088/2,1)<<",ch2="<<image->at(2320/2,3088/2,2)<<std::endl;
timer.reset();

cv::Mat imgMat=image->toCvMat();
std::cout<<"convert time="<<timer.get_elapsed()<<"ms."<<std::endl;
timer.reset();
cv::imshow("imgMat",imgMat);
cv::waitKey();

timer.reset();
mve::image::save_file(image,"./teset.png");
std::cout<<"save time="<<timer.get_elapsed()<<"ms."<<std::endl;

return 0;
}