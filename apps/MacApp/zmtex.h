#include <fstream>
#include <iostream>
#include <vector>

#include "util/file_system.h"
#include "util/system.h"
#include "util/timer.h"
#include "mve/mesh_io_obj.h"

#include "debug.h"
#include "progress_counter.h"
#include "texturing.h"
#include "timer.h"
#include "util.h"

#include "texArguments.h"
int texrecon(int argc, const char **argv);
