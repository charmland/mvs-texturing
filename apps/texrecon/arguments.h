/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef ARGUMENTS_HEADER
#define ARGUMENTS_HEADER

#include "tex/settings.h"
#include "util/arguments.h"

/** Struct containing the commandline arguments at runtime. */
struct TexArguments {
  /**
   * 输入的场景.可以是目录或mve特定的文件格式.
   * 目录的话，就是包含一序列.cam和图片
   */
  std::string in_scene;
  /**
   * 输入的mesh文件
   */
  std::string in_mesh;
  /**
   * 输出目录
   */
  std::string out_prefix;
  /**
   * 输入的数据代价文件[可选]
   */
  std::string data_cost_file;
  /**
   * 输入的标签组合[可选]
   */
  std::string labeling_file;
  /**
   * 算法组件配置
   */
  tex::Settings settings;
  /**
   * 写出算法各个组件的耗时统计
   */
  bool write_timings;
  /**
   * 写出中间结果
   */
  bool write_intermediate_results;
  /**
   * 写出视图选择的结果.默认为true.
   */
  bool write_view_selection_model;

  /** Returns a muliline string of the current arguments. */
  std::string to_string();
};

/**
 * Parses the commandline arguments.
 * @throws std::invalid_argument
 */
TexArguments parse_args(int argc, char **argv);

#endif /* ARGUMENTS_HEADER */
