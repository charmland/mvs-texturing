/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <fstream>
#include <iostream>
#include <vector>

#include <mve/mesh_io_ply.h>
#include <util/file_system.h>
#include <util/system.h>
#include <util/timer.h>
#include <mve/mesh_io_obj.h>
#include <cstdlib>

#include "tex/debug.h"
#include "tex/progress_counter.h"
#include "tex/texturing.h"
#include "tex/timer.h"
#include "tex/util.h"
#include "arguments.h"
#include "tex/facial_feature_prior.h"

#include "tbb/task_scheduler_init.h"
// forward declaration

using namespace TEX_NAMESPACE;

int main(int argc, char **argv) {
  util::system::print_build_timestamp(argv[0]);
  util::system::register_segfault_handler();

  Timer timer;
  util::WallTimer wtimer;
  //============解析输入参数、加载资源==========
  TexArguments conf;
  try {
    conf = parse_args(argc, argv);
  } catch (std::invalid_argument &ia) {
    std::cerr << ia.what() << std::endl;
    std::exit(EXIT_FAILURE);
  }
  // out_dir是out_prefix的父级目录
  std::string const out_dir = util::fs::dirname(conf.out_prefix);
  if (!util::fs::dir_exists(out_dir.c_str())) {
    std::cerr << "Destination directory does not exist!" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  ///=>创建临时输出目录
  std::string const tmp_dir = util::fs::join_path(out_dir, "tmp");
  if (!util::fs::dir_exists(tmp_dir.c_str())) {
    util::fs::mkdir(tmp_dir.c_str());
  } else {
    std::cerr << "Temporary directory \"tmp\" exists within the destination "
                 "directory.\nDelete it and remake it now.\n"
              << std::endl;
    util::fs::rmdir(tmp_dir.c_str());
    util::fs::mkdir(tmp_dir.c_str());
    // std::exit(EXIT_FAILURE);
  }

#if defined(TBB_EXPLICIT_INIT)
  //指定线程数=1时,可模拟单线程运行,便于排查问题
  tbb::task_scheduler_init tbbInit(1);
#endif

  ///=>加载mesh文件
  std::cout << "Load and prepare mesh: " << std::endl;
  mve::TriangleMesh::Ptr mesh;
  try {
    // mesh = mve::geom::load_ply_mesh(conf.in_mesh);
    mesh = mve::geom::load_obj_mesh(conf.in_mesh);
  } catch (std::exception &e) {
    std::cerr << "\tCould not load mesh: " << e.what() << std::endl;
    std::exit(EXIT_FAILURE);
  }
  ///=>计算拓扑信息.每个顶点的类型（普通、边缘、非流形、孤立)，邻接点，邻接面
  mve::MeshInfo mesh_info(mesh);
  ///=>移除冗余面、确保有点法向和面法向
  tex::prepare_mesh(&mesh_info, mesh);

  ///=>读取场景目录，构建视图列表
  std::cout << "Generating texture views: " << std::endl;
  tex::TextureViews texture_views;
  tex::generate_texture_views(conf.in_scene, &texture_views, tmp_dir);
  //=>把算法配置写到.conf文件
  write_string_to_file(conf.out_prefix + ".conf", conf.to_string());
  timer.measure("Loading");

  //==============视图选择===============
  std::size_t const num_faces = mesh->get_faces().size() / 3;
  ///=>构建face-face邻接图
  std::cout << "Building adjacency graph: " << std::endl;
  tex::Graph graph(num_faces);
  tex::build_adjacency_graph(mesh, mesh_info, &graph);

  if (conf.labeling_file.empty()) {
    ///=>计算data cost
    std::cout << "View selection:" << std::endl;
    util::WallTimer rwtimer;
    // FN列xIN行
    tex::DataCosts data_costs(num_faces, texture_views.size());
    if (conf.data_cost_file.empty()) {
      //构造face prior
      std::size_t frontal_view_id = get_frontal_face_view_index(texture_views);
      TextureView &frontal_view = texture_views[frontal_view_id];
      FacialFeatureRectPtr facePrior =
          construct_facial_feature_rects(frontal_view);
      //计算数据代价表
      tex::calculate_data_costs(mesh, &texture_views, conf.settings,
                                &data_costs, frontal_view_id, facePrior);
      if (conf.write_intermediate_results) {
        // for debug，写data cost文件
        std::cout << "\tWriting data cost file... " << std::flush;
        tex::DataCosts::save_to_file(data_costs,
                                     conf.out_prefix + "_data_costs.spt");
        std::cout << "done." << std::endl;
      }
    } else {
      //从文件中加载data cost
      std::cout << "\tLoading data cost file... " << std::flush;
      try {
        tex::DataCosts::load_from_file(conf.data_cost_file, &data_costs);
      } catch (util::FileException e) {
        std::cout << "failed!" << std::endl;
        std::cerr << e.what() << std::endl;
        std::exit(EXIT_FAILURE);
      }
      std::cout << "done." << std::endl;
    }
    timer.measure("Calculating data costs");
    ///=>构造MRF并求解
    try {
      tex::view_selection(data_costs, &graph, conf.settings);
    } catch (std::runtime_error &e) {
      std::cerr << "\tOptimization failed: " << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
    timer.measure("Running MRF optimization");
    std::cout << "\tTook: " << rwtimer.get_elapsed_sec() << "s" << std::endl;

    /* Write labeling to file. */
    if (conf.write_intermediate_results) {
      std::vector<std::size_t> labeling(graph.num_nodes());
      for (std::size_t i = 0; i < graph.num_nodes(); ++i) {
        labeling[i] = graph.get_label(i);
      }
      vector_to_file(conf.out_prefix + "_labeling.vec", labeling);
    }
  } // end if labeling_file.empty()
  else {
    //--labeling_file=/media/tensorflow/Samsung_T3/MeshData/1530497622.658779-texrecon/texrecon/out-lambda0.025-fix/texrecon_out_labeling.vec
    std::cout << "Loading labeling from file... " << std::flush;
    /* Load labeling from file. */
    std::vector<std::size_t> labeling =
        vector_from_file<std::size_t>(conf.labeling_file);
    if (labeling.size() != graph.num_nodes()) {
      std::cerr
          << "Wrong labeling file for this mesh/scene combination... aborting!"
          << std::endl;
      std::exit(EXIT_FAILURE);
    }

    /* Transfer labeling to graph. */
    for (std::size_t i = 0; i < labeling.size(); ++i) {
      const std::size_t label = labeling[i];
      if (label > texture_views.size()) {
        std::cerr
            << "corrupt label file for this mesh/scene combination.aborting!"
            << std::endl;
        std::exit(EXIT_FAILURE);
      }
      graph.set_label(i, label);
    }
    std::cout << "done." << std::endl;
  } // end if conf.labeling_file.empty()

  //=================create atlas===================
  tex::TextureAtlases texture_atlases;
  {
    /* Create texture patches and adjust them. */
    //全局patch列表，包含未分配标签的patch.
    tex::TexturePatches texture_patches;
    tex::VertexProjectionInfos vertex_projection_infos;
    std::cout << "Generating texture patches:" << std::endl;
    tex::generate_texture_patches(graph, mesh, mesh_info, &texture_views,
                                  conf.settings, &vertex_projection_infos,
                                  &texture_patches);
    //=====global leveling
    if (conf.settings.global_seam_leveling) {
      std::cout << "Running global seam leveling:" << std::endl;
      /*tex::global_seam_leveling(graph, mesh, mesh_info,
         vertex_projection_infos, &texture_patches);*/
      tex::global_seam_leveling_fix(graph, mesh, mesh_info,
                                    vertex_projection_infos, &texture_patches);
      // tex::global_seam_leveling_eig2acc(graph, mesh, mesh_info,
      // vertex_projection_infos,
      //  &texture_patches);

      timer.measure("Running global seam leveling");
    } else {
      ProgressCounter texture_patch_counter(
          "Calculating validity masks for texture patches",
          texture_patches.size());
      //#pragma omp parallel for schedule(dynamic)
      for (std::size_t i = 0; i < texture_patches.size(); ++i) {
        texture_patch_counter.progress<SIMPLE>();
        TexturePatch::Ptr texture_patch = texture_patches[i];
        std::vector<math::Vec3f> patch_adjust_values(
            texture_patch->get_faces().size() * 3, math::Vec3f(0.0f));
        texture_patch->adjust_colors(patch_adjust_values);
        texture_patch_counter.inc();
      }
      timer.measure("Calculating texture patch validity masks");
    }
    //======local leveling
    if (conf.settings.local_seam_leveling) {
      std::cout << "Running local seam leveling:" << std::endl;
      tex::local_seam_leveling(graph, mesh, vertex_projection_infos,
                               &texture_patches);
    }
    timer.measure("Running local seam leveling");

    /* Generate texture atlases. */
    std::cout << "Generating texture atlases:" << std::endl;
    tex::generate_texture_atlases(&texture_patches, conf.settings,
                                  &texture_atlases);
  }

  /* Create and write out obj model. */
  {
    std::cout << "Building objmodel:" << std::endl;
    tex::Model model;
    tex::build_model(mesh, texture_atlases, &model);
    timer.measure("Building OBJ model");

    std::cout << "\tSaving model... " << std::flush;
    tex::Model::save(model, conf.out_prefix);
    std::cout << "done." << std::endl;
    timer.measure("Saving");
  }

  std::cout << "Whole texturing procedure took: " << wtimer.get_elapsed_sec()
            << "s" << std::endl;
  timer.measure("Total");
  if (conf.write_timings) {
    timer.write_to_file(conf.out_prefix + "_timings.csv");
  }

  if (conf.write_view_selection_model) {
    texture_atlases.clear();
    std::cout << "Generating debug texture patches:" << std::endl;
    {
      tex::TexturePatches texture_patches;
      generate_debug_embeddings(&texture_views);
      tex::VertexProjectionInfos
          vertex_projection_infos; // Will only be written
      tex::generate_texture_patches(graph, mesh, mesh_info, &texture_views,
                                    conf.settings, &vertex_projection_infos,
                                    &texture_patches);
      tex::generate_texture_atlases(&texture_patches, conf.settings,
                                    &texture_atlases);
    }

    std::cout << "Building debug objmodel:" << std::endl;
    {
      tex::Model model;
      tex::build_model(mesh, texture_atlases, &model);
      std::cout << "\tSaving model... " << std::flush;
      tex::Model::save(model, conf.out_prefix + "_view_selection");
      std::cout << "done." << std::endl;
    }
  }

  /* Remove temporary files. */
  for (util::fs::File const &file : util::fs::Directory(tmp_dir)) {
    util::fs::unlink(util::fs::join_path(file.path, file.name).c_str());
  }
  util::fs::rmdir(tmp_dir.c_str());

  return EXIT_SUCCESS;
}
