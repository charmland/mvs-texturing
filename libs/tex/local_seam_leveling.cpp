/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

//#include "tbb/tbb.h"

#include <math/accum.h>

#include "progress_counter.h"
#include "texturing.h"
#include "seam_leveling.h"

TEX_NAMESPACE_BEGIN

#define STRIP_SIZE 20
/**
 * 计算某一条seam edge上某一个样本点在两个半边投影颜色的均值.
 * 忽略此seam edge在label0的投影.
 *
 * @note 若此seam edge中有一个邻接patch标签为0，则忽略它在这个patch中的投影.
 *
 * @param edge_projection_infos 一条边所对应多个patch
 * @param texture_patches 全局调色后的纹理
 * @param t 遍历缝隙边颜色 个数 与 2倍缝隙边长 比例（0，1）
 * @return 缝隙上的点 颜色值
 */
math::Vec3f
mean_color_of_edge_point(
              std::vector<EdgeProjectionInfo> const & edge_projection_infos,
              std::vector<TexturePatch::Ptr> const & texture_patches, float t) {
  assert(0.0f <= t && t <= 1.0f /*&&edge_projection_infos.size()==2*/);
  math::Accum<math::Vec3f> color_accum(math::Vec3f(0.0f));
  for (EdgeProjectionInfo const & edge_projection_info : edge_projection_infos) {
      TexturePatch::Ptr texture_patch = texture_patches[edge_projection_info.texture_patch_id];
      if (texture_patch->get_label() == 0) continue;
      math::Vec2f pixel = edge_projection_info.p1 * t + (1.0f - t) * edge_projection_info.p2;
      math::Vec3f color = texture_patch->get_pixel_value(pixel);
      color_accum.add(color, 1.0f);
  }
  math::Vec3f mean_color = color_accum.normalized();
  return mean_color;
}

/**
 *
 * @param p1:line.from
 * @param p2:line.to
 * @param edge_color: *line.color
 * @param texture_patch
 */
void
draw_line(math::Vec2f p1, math::Vec2f p2,
    std::vector<math::Vec3f> const & edge_color,
    TexturePatch::Ptr texture_patch) {

  /**布雷森汉姆直线算法 http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm */
  //两个端点都是四舍五入
  int x0 = std::floor(p1[0] + 0.5f);
  int y0 = std::floor(p1[1] + 0.5f);
  int const x1 = std::floor(p2[0] + 0.5f);
  int const y1 = std::floor(p2[1] + 0.5f);

  float tdx = static_cast<float>(x1 - x0);
  float tdy = static_cast<float>(y1 - y0);
  float length = std::sqrt(tdx * tdx + tdy * tdy);

  int const dx = std::abs(x1 - x0);
  int const dy = std::abs(y1 - y0);
  int const sx = x0 < x1 ? 1 : -1;
  int const sy = y0 < y1 ? 1 : -1;
  int err = dx - dy;

  int x = x0;
  int y = y0;
  while (true) {
    math::Vec2i pixel(x, y);

    tdx = static_cast<float>(x1 - x);
    tdy = static_cast<float>(y1 - y);

    /* If the length is zero we sample the midpoint of the projected edge. */
    float t = (length != 0.0f) ? std::sqrt(tdx * tdx + tdy * tdy) / length : 0.5f;

    math::Vec3f color;
    if (t < 1.0f && edge_color.size() > 1) {
        std::size_t idx = std::floor(t * (edge_color.size() - 1));
        color = (1.0f - t) * edge_color[idx] + t * edge_color[idx + 1];
    } else {
        color = edge_color.back();
    }

    ///给直线 添颜色
    texture_patch->set_pixel_value(pixel, color);
    if (x == x1 && y == y1)
        break;

    int const e2 = 2 * err;
    if (e2 > -dy) {
        err -= dy;
        x += sx;
    }
    if (e2 < dx) {
        err += dx;
        y += sy;
    }
  }
}

struct Pixel {
    math::Vec2i pos;
    math::Vec3f const * color;
};

/**
 * 一条seam edge在某个patch中的投影采样信息.
 *
 * 一条seam edge有两个半边投影采样结果(即使它与label0 patch邻接):
 *
 * 1.在某个patch中的起点投影坐标
 *
 * 2.在某个patch中的终点投影坐标
 *
 * 3.颜色样本序列(两个半边的颜色平均贡献给每个样本,除label0 patch外)
 */
struct Line {
    math::Vec2i from;
    math::Vec2i to;
    /**
     * 指向一个数组.数组中记录了此Line对应的
     */
    std::vector<math::Vec3f> const * color;
};

/**
 * 在mesh中找到所有的seam edges,然后每条都拆成两个半边,找出它们的半边
 * 投影信息EdgeProjectionInfo.
 *
 * //todo:在global_seam_leveling阶段构造A矩阵时可以顺手构造全局半边投影信息.
 * @param graph
 * @param mesh
 * @param vertex_projection_infos
 * @param edge_projection_infos
 * @return
 */
inline size_t find_all_seam_edge_projections(
          UniGraph const &graph,
          mve::TriangleMesh::ConstPtr &mesh,
          VertexProjectionInfos const &vertex_projection_infos,
          std::vector<std::vector<EdgeProjectionInfo>> &edge_projection_infos){

  std::vector<MeshEdge> seam_edges;
  find_seam_edges(graph, mesh, &seam_edges);
  edge_projection_infos.resize(seam_edges.size());
  for (std::size_t i = 0; i < seam_edges.size(); ++i) {
    MeshEdge const & seam_edge = seam_edges[i];
    //通过点2投影信息,找到边的2d投影信息（同patch，同三角面）三样：patch_id，两个顶点2d信息
    find_mesh_edge_projections(vertex_projection_infos, seam_edge,
        &edge_projection_infos[i]);
  }
  return seam_edges.size();
}

void
local_seam_leveling(UniGraph const & graph,
    mve::TriangleMesh::ConstPtr mesh,
    VertexProjectionInfos const & vertex_projection_infos,
    TexturePatches * texture_patches) {

    using namespace std; using namespace tbb;
    size_t const VN = vertex_projection_infos.size();
    //记录每个边界顶点的投影平均颜色
    vector<math::Vec3f> vertex_colors(VN);
    //记录每个patch的边线上顶点的[投影采样信息]
    vector<vector<Pixel> > pixels(texture_patches->size());
    //所有seam edge上采样的颜色样本集合
    vector<vector<math::Vec3f> > edge_colors;
    //记录每个patch中的边线[投影采样信息]
    vector<vector<Line> > lines(texture_patches->size());

    //记录每条seam edge的半边投影信息
    vector<vector<EdgeProjectionInfo> > edge_projection_infos;
    size_t seam_edge_count= find_all_seam_edge_projections(graph, mesh,
        vertex_projection_infos, edge_projection_infos);
    edge_colors.resize(seam_edge_count);

    /*Sample edge colors. 遍历每条seam edge.*/
    for (size_t i = 0; i < edge_projection_infos.size(); ++i) {
      /* Determine sampling (ensure at least two samples per edge).*/
      float max_length = 1;
      /*seam edge的两条半边中的最长投影边确定了edge_colors[i]的样本数*/
      for (EdgeProjectionInfo const & edge_projection_info : edge_projection_infos[i]) {
        float length = (edge_projection_info.p1 - edge_projection_info.p2).norm();
        max_length = max(max_length, length);
      }
      vector<math::Vec3f> & edge_color = edge_colors[i];
      edge_color.resize(ceil(max_length * 2.0f));
      for (size_t j = 0; j < edge_color.size(); ++j) {
        float t = static_cast<float>(j) / (edge_color.size() - 1);
        //这里两个半边中若有一个是投影到label0 patch则忽略它对样本的贡献
        edge_color[j] = mean_color_of_edge_point(edge_projection_infos[i], *texture_patches, t);
      }
      /*分别记录当前seam edge两个半边的投影信息到lines数组.每个Line的颜色样本却是平均采样的*/
      for (EdgeProjectionInfo const & edge_projection_info : edge_projection_infos[i]) {
        Line line;
        //端点位置四舍五入
        line.from = edge_projection_info.p1 + math::Vec2f(0.5f, 0.5f);
        line.to = edge_projection_info.p2 + math::Vec2f(0.5f, 0.5f);
        line.color = &edge_colors[i];
        lines[edge_projection_info.texture_patch_id].push_back(line);
      }
    }

    /* Sample patch border vertex colors. */
    for (size_t i = 0; i < vertex_colors.size(); ++i) {
      vector<VertexProjectionInfo> const & projection_infos = vertex_projection_infos[i];
      //忽略patch内部的顶点
      if (projection_infos.size() <= 1) continue;
      math::Accum<math::Vec3f> color_accum(math::Vec3f(0.0f));
      for (VertexProjectionInfo const & projection_info : projection_infos) {
          //累加该顶点在所有patch中的投影点的颜色,忽略label0 patch
          TexturePatch::Ptr texture_patch = texture_patches->at(projection_info.texture_patch_id);
          if (texture_patch->get_label() == 0) continue;
          math::Vec3f color = texture_patch->get_pixel_value(projection_info.projection);
          color_accum.add(color, 1.0f);
      }
      if (color_accum.w == 0.0f) continue;
      vertex_colors[i] = color_accum.normalized();

      for (VertexProjectionInfo const & projection_info : projection_infos) {
          Pixel pixel;
          //像素位置是四舍五入?
          pixel.pos = math::Vec2i(projection_info.projection + math::Vec2f(0.5f, 0.5f));
          pixel.color = &vertex_colors[i];
          pixels[projection_info.texture_patch_id].push_back(pixel);
      }
    }

    ProgressCounter texture_patch_counter("\tBlending texture patches", texture_patches->size());
  #ifndef SERIAL_BLENDING
    //#pragma omp parallel for schedule(dynamic)
    parallel_for(blocked_range<size_t>(0,texture_patches->size()),[&texture_patches,&pixels,&lines](const blocked_range<size_t>&r){for(auto i=r.begin(); i!=r.end();i++){
  #else
    ({for (size_t i = 0; i < texture_patches->size(); ++i) {
  #endif
      TexturePatch::Ptr texture_patch = texture_patches->at(i);
      mve::FloatImage::Ptr image = texture_patch->get_image()->duplicate();
      //todo:在这里把image显示出来,看看draw_line之前和之后
      /* Apply colors. */
      for (Pixel const &pixel : pixels[i]) {
        texture_patch->set_pixel_value(pixel.pos, *pixel.color);
      }

      //按照颜色画线，blending_mask添色后 赋值128
      for (Line const &line : lines[i]) {
        draw_line(line.from, line.to, *line.color, texture_patch);
      }
      texture_patch_counter.progress<SIMPLE>();
      /*Only alter a small strip of texture patches originating from input images*/
      if (texture_patch->get_label() != 0) {
        texture_patch->prepare_blending_mask(STRIP_SIZE);
      }

      texture_patch->blend(image);
      texture_patch->release_blending_mask();
      texture_patch_counter.inc();
    }
  });
}

TEX_NAMESPACE_END
