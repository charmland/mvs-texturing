// facial_feature_prior.h.hpp
// Created by chenglong on 2018/8/17.
// Copyright © 2018 www.zhimei.ai All rights reserved.
//
#ifndef FACIAL_FEATURE_PRIOR_H
#define FACIAL_FEATURE_PRIOR_H

#include "defines.h"
#include "texture_view.h"
#include "texturing.h"

TEX_NAMESPACE_BEGIN

int get_frontal_face_view_index(const TextureViews &views);

FacialFeatureRectPtr construct_facial_feature_rects(
        const TextureView &frontal_view);


TEX_NAMESPACE_END

#endif //FACIAL_FEATURE_PRIOR_H