//
// Created by 程龙 on 2018/8/8.
//

#include "view_io.h"
#include "../exception/TexException.h"
#include <opencv2/opencv.hpp>
#include <PMP/common/FaceLandmark.hpp>

TEX_NAMESPACE_BEGIN

bool readCameraFile(const std::string &camFile, mve::CameraInfo &cam) {
  /**
   * InfiniTAM ios app中ViewController.mm writeKeyFrame函数写入的相机参数
   *是深度相机的参数，深度相机图像已对齐到了同步获取的彩色图像,因此二者内外参一致，
   *(但其实仍有误差)都可看作是彩色图像的内外参.但若彩色图像是静态捕获的高清大图,
   *则可能需要记录捕获高清大图时的相机内参,并使用该内参.
   *由于深度相机的图像，已对齐到同尺寸的彩色图像，因此可认为深
   * 度相机的外参就是彩色相机的外参.
   * 当前相机内外参文件的内容格式如下:
   * t0_d t1_d t2_d     #深度图的外参-平移部分
   * r00_d r01_d r02_d  #深度图的外参-旋转部分，行优先存储r00 r01 r02是R的第一行
   * r10_d r11_d r12_d
   * r20_d r21_d r22_d
   * w_d h_d                #深度图的尺寸
   * fx_d fy_d cx_d cy_d    #深度图的内参(未标准化)

   * w_hc h_hc               #高清大图的尺寸
   * fx_hc fy_hc cx_h cy_hc  #高清大图的内参(未单位化)
   */
  try {
    std::ifstream camInput(camFile);
    if (camInput.fail()) {
      throw TexException::getPredefinedByCode(TexErrorCodeEnum::E_KF_READ_cam).setUIMsg("无法打开相机参数文件");
    }
    //==>外参
    camInput >> cam.trans[0] >> cam.trans[1] >> cam.trans[2];
    camInput >> cam.rot[0] >> cam.rot[1] >> cam.rot[2];
    camInput >> cam.rot[3] >> cam.rot[4] >> cam.rot[5];
    camInput >> cam.rot[6] >> cam.rot[7] >> cam.rot[8];
    //==>深度图内参和尺寸
    int width_d = 0, height_d = 0;
    float fx_d = 0, fy_d = 0, cx_d = 0, cy_d = 0;
    camInput >> width_d >> height_d;
    camInput >> fx_d >> fy_d >> cx_d >> cy_d;
    //      cam.paspect = fy_d / fx_d; //像素宽/像素高度,正方形像素此值为1
    //      float imgAspect =
    //          ((float)width_d) / height_d * cam.paspect; //图像物理宽高比
    //      if (imgAspect < 1.0f) { // Potrait,高为长边
    //        cam.flen = fy_d / cam.height_d;
    //      } else { // Landscape,宽为长边
    //        cam.flen = fx_d / cam.width_d;
    //      }
    //      cam.ppoint[0] =
    //          cx_d / cam.width_d;
    //          //像平面主点的x坐标，已标准化，与图像尺寸无关
    //      cam.ppoint[1] =
    //          cy_d / cam.height_d; //像平面主点的y坐标，已标准，与图像尺寸无关
    //      if (std::isnan(cam.flen)) {
    //        return -2;
    //      }
    //==>(高清)彩色图的内参和尺寸.
    if (!camInput.eof()) {
      int width_hc = 0, height_hc = 0;
      float fx_hc = 0, fy_hc = 0, cx_hc = 0, cy_hc = 0;
      camInput >> width_hc >> height_hc;
      camInput >> fx_hc >> fy_hc >> cx_hc >> cy_hc;
      cam.paspect = fy_hc / fx_hc; //彩色图的每个像素宽高比
      float imageAspect = ((float)width_hc) / height_hc * cam.paspect;
      if (imageAspect < 1.f) { // Potrait
        cam.flen = fy_hc / height_hc;
      } else { // Landscape
        cam.flen = fx_hc / width_hc;
      }
      cam.ppoint[0] = cx_hc / width_hc;
      cam.ppoint[1] = cy_hc / height_hc;
      if (std::isnan(cam.flen)) {
        throw TexException::getPredefinedByCode(TexErrorCodeEnum::E_KF_DATA_cam);
      }
      //彩图的畸变因子暂时拿不到
      cam.dist[0]=0.f;cam.dist[1]=0.f;
    }
    #ifdef TEXTUREVIEW_STAT
    cam.debug_print();
    #endif
    camInput.close();
  } catch (std::ios_base::failure &fe) {
    throw TexException::getPredefinedByCode(TexErrorCodeEnum::E_KF_READ_cam).setLogMsg(fe.what());
  }
  return true;
}


std::tuple<float ,float,float> readLdmk2dArr(const std::string &ldmkFilePath,
    TextureView::Landmark2DArrPtr &ldmk2ds){
  using tex::TexException;
  using tex::TexErrorCodeEnum;
  cv::Mat ldmks2dMat;
  float pitch=0, yaw=0, roll=0;
  try {
    cv::FileStorage ldmkFile(ldmkFilePath, cv::FileStorage::READ);
    if (!ldmkFile.isOpened()) {
      throw TexException::getPredefinedByCode(TexErrorCodeEnum::E_KF_READ_ldmk)
          .setLogMsg("open failed");
    }
    ldmkFile["ldmk2d"] >> ldmks2dMat;
    //ldmkFile["pitch"]>>pitch;
    ldmkFile["yaw"] >> yaw;
    //ldmkFile["roll"]>>roll;
    ldmkFile.release();
  } catch (...) {
    throw TexException::getPredefinedByCode(TexErrorCodeEnum::E_KF_READ_ldmk)
        .setLogMsg(ldmkFilePath);
  }
  //检查特征点数量是否缺失-unlikely
  if (ldmks2dMat.rows < FaceLandmark::LANDMARK_COUNT) {
    throw TexException::getPredefinedByCode(TexErrorCodeEnum::E_KF_DATA_ldmk);
  }
  //有多少特征点全部读上来.
  ldmk2ds.reset(new TextureView::Landmark2DArr());
  ldmk2ds->reserve(ldmks2dMat.rows);
  for (int i = 0; i < FaceLandmark::LANDMARK_COUNT; i++) {
    ldmk2ds->emplace_back(ldmks2dMat.at<float>(i, 0), ldmks2dMat.at<float>(i, 1));
  }
  return std::make_tuple(pitch,yaw,roll);
}

TEX_NAMESPACE_END
