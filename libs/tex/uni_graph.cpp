/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <limits>
#include <list>

#include "uni_graph.h"

TEX_NAMESPACE_BEGIN

UniGraph::UniGraph(std::size_t nodes) {
  adj_lists.resize(nodes);
  labels.resize(nodes);
  edges = 0;
}

/**
 * 在此UniGraph中找出所有标签为label的联通子图(可能有多个).
 * 特别的，label为0的子图中都是未成功分配到标签的三角面
 * @param label 标签值
 * @param subgraphs 返回具有标签值为label的所有联通子图.
 */
void UniGraph::get_subgraphs(
    std::size_t label, std::vector<std::vector<std::size_t>> *subgraphs) const {
  //用于在遍历过程中，记录每个节点是否已访问到
  std::vector<bool> used(adj_lists.size(), false);
  //遍历每个三角面，找出联通子图
  for (std::size_t i = 0; i < adj_lists.size(); ++i) {
    if (labels[i] == label && !used[i]) {
      //新建一个空的联通子图，加入到subgraphs中
      subgraphs->push_back(std::vector<std::size_t>());

      std::list<std::size_t> queue;

      queue.push_back(i);//fid为i的三角面加入到队列中构成此联通子图的初始节点
      used[i] = true;//fid为i的三角面已被访问到,标记为已访问

      while (!queue.empty()) {
        std::size_t node = queue.front();
        queue.pop_front();

        subgraphs->back().push_back(node);

        /* BFS:Add all unused neighbours with the same label to the queue. */
        std::vector<std::size_t> const &adj_list = adj_lists[node];
        for (std::size_t j = 0; j < adj_list.size(); ++j) {
          std::size_t adj_node = adj_list[j];
          assert(adj_node < labels.size() && adj_node < used.size());
          if (labels[adj_node] == label && !used[adj_node]) {
            queue.push_back(adj_node);
            used[adj_node] = true;
          }
        }//end for BFS
      }//end while
    }//end if
  }//end for i<adj_list.size()
}

TEX_NAMESPACE_END
