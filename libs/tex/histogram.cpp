/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <algorithm>
#include <cassert>
#include <cerrno>
#include <cmath>
#include <cstring>
#include <fstream>

#include <util/exception.h>
#include <util/file_system.h>

#include "histogram.h"

TEX_NAMESPACE_BEGIN

Histogram::Histogram(float _min, float _max, std::size_t num_bins)
    : min(_min), max(_max), num_values(0) {
  bins.resize(num_bins);
}

void Histogram::add_value(float value) {
  float clamped_value = std::max(min, std::min(max, value));
  /*
   * 令p=(max-min)/(n-1),这是区间bin的步长,也是此柱状图的精度,一个value如果跨不过某个区间步长,就只能落在该区间.
   * bins中每个区间定义如下：
   *     b<0>         b<1>           b<2>                b<n-1>
   * [min,min+p) [min+p,min+2p) [min+2p,min+3p) ... [min+(n-1)p,min+(n-1)p]
   * 使用如下公式计算某个value所在的bins的索引:
   * index=floor((value-min)/(max-min)*(n-1))
   */
  std::size_t index =
      floor(((clamped_value - min) / (max - min)) * (bins.size() - 1));
  assert(index < bins.size());
  bins[index]++;
  ++num_values;
}

void Histogram::save_to_file(std::string const &filename) const {
  std::ofstream out(filename.c_str());
  if (!out.good())
    throw util::FileException(filename, std::strerror(errno));

  out << "Bin, Values" << std::endl;
  for (std::size_t i = 0; i < bins.size(); ++i) {
    out << i << ", " << bins[i] << std::endl;
  }
  out.close();
}

float Histogram::get_approx_percentile(float percentile) const {
  assert(percentile >= 0.0f && percentile <= 1.0f);

  int num = 0;
  float upper_bound = min;
  for (std::size_t i = 0; i < bins.size(); ++i) {
    //这里应该是≥吧
    if (static_cast<float>(num) / num_values > percentile)
      return upper_bound;

    num += bins[i];
    //p=(max-min)/(n-1),upper_bound=i*p+min
    upper_bound =
        (static_cast<float>(i) / (bins.size() - 1)) * (max - min) + min;
  }
  return max;
}

TEX_NAMESPACE_END
