//
// Created by 程龙 on 2018/8/8.
//

#ifndef TEXTURING_VIEW_IO_HPP
#define TEXTURING_VIEW_IO_HPP
#include <string>
#include <fstream>
#include <cmath>
#include "mve/camera.h"
#include "defines.h"
#include "texture_view.h"

TEX_NAMESPACE_BEGIN
/**
 * ZMScan输出的.cam文件中包含一份外参，一份深度图内参和一份彩色图内参.
 * 在texrecon中目前只需要读取其中的外参和彩色图内参
 * @param camFile .cam文件路径
 * @param [out] cam 待填充的相机参数
 * @return
 */
bool readCameraFile(const std::string &camFile, mve::CameraInfo &cam);

 /**
  * 读取二维特征点文件.
  * @param ldmkFilePath
  * @param ldmk2ds
  * @return 返回欧拉角
  */
std::tuple<float ,float,float> readLdmk2dArr(const std::string &ldmkFilePath,
    TextureView::Landmark2DArrPtr &ldmk2ds);

TEX_NAMESPACE_END
#endif // TEXTURING_VIEW_IO_HPP
