// global_seam_leveling_fix.cpp.cpp
// Created by chenglong on 2018/8/25.
// Copyright © 2018 www.zhimei.ai All rights reserved.
//

/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <map>
#include <set>

#include <util/timer.h>
#include <math/accum.h>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#ifdef __APPLE__
#pragma message("we're using Apple's Toolchain to compile")
#include <Accelerate/Accelerate.h>
#endif
#include <fstream>
#include <iostream>
#include <future>

#include "texturing.h"
#include "seam_leveling.h"
#include "progress_counter.h"
#include "tbb/tbb.h"

TEX_NAMESPACE_BEGIN

typedef Eigen::SparseMatrix<float> SpMat;
enum EigenSpMatTriangle{
  EigenSpMatTriangle_Lower,
  EigenSpMatTriangle_Upper,
  EigenSpMatTriangle_None
};
//打印稀疏矩阵内容
void debug_output_eigen_spmat(SpMat &Lhs,EigenSpMatTriangle triangle,
                              const std::string&file);

/**论文图4 线性权重计算相邻点 颜色值*/
math::Vec3f sample_edge(TexturePatch::ConstPtr texture_patch,
                        math::Vec2f p1,
                        math::Vec2f p2) {
  math::Vec2f p12 = p2 - p1;
  //样本个数 = 向量模长* 2（像素级）（论文8页倒数6行,采样密度是不是有点高?）
  std::size_t num_samples = std::max(p12.norm(), 1.0f) * 2.0f;

  math::Accum<math::Vec3f> color_accum(math::Vec3f(0.0f));
  /* 用线性权重对进行采样Sample the edge with linear weights. */
  for (std::size_t s = 0; s < num_samples; ++s) {
    float fraction = static_cast<float>(s) / (num_samples - 1);
    math::Vec2f sample_point = p1 + p12 * fraction;
    math::Vec3f color(texture_patch->get_pixel_value(sample_point));
    color_accum.add(color, 1.0f - fraction); /// 1-fraction = 权重
  }
  return color_accum.normalized();
}

/**
 * 计算当前顶点Vi的色差.
 *
 * 顶点Vi落在label1和label2分界线上，它被拆分为label1和label2副本，此函数可计算Vi两个
 * 副本的颜色差.为了减少单个顶点投影采样带来的误差，这里计算Vi邻接边在label1和label2中的
 * 投影线段的颜色的加权求和，作为顶点Vi两个副本的颜色.
 *
 * //todo:考虑在这一步填充一个全局半边投影信息表.
 * @param vertex_projection_infos
 * @param mesh
 * @param graph FFAdj,可查到每个三角面所属的patch id
 * @param texture_patches
 * @param seam_edges 与Vi邻接的边，是label1和label2的分界线上所有与Vi直接相连的边,
 *        若patch划分比较复杂，则这里可能有多条边.
 * @param label1
 * @param label2
 * @return
 */
math::Vec3f calculate_difference(VertexProjectionInfos const &vertex_projection_infos,
                     mve::TriangleMesh::VertexList const &vertices,
                     UniGraph const & graph,
                     std::vector<TexturePatch::Ptr> const &texture_patches,
                     std::vector<SeamEdge> const &seam_edges,
                     const int label1, const int label2) {

  using std::cout; using std::endl;
  assert(label1 != 0 && label2 != 0 && label1 < label2);
  assert(!seam_edges.empty());
  #ifdef GLOBAL_SEAM_DEBUG
  if (seam_edges.size()>2){
    cout<<"warn:complex seam edges:";
    for(auto &e:seam_edges){
      cout<<"(v"<<e.v1<<",v"<<e.v2<<")";
    }
    cout<<endl;
  }
  #endif
  //mve::TriangleMesh::VertexList const &vertices = mesh->get_vertices();

  math::Accum<math::Vec3f> color1_accum(math::Vec3f(0.0f));
  math::Accum<math::Vec3f> color2_accum(math::Vec3f(0.0f));
  //通常seam_edges中边数也应该不超过2，除非此顶点
  for (SeamEdge const &seam_edge : seam_edges) {
    math::Vec3f v1 = vertices[seam_edge.v1];
    math::Vec3f v2 = vertices[seam_edge.v2];
    float length = (v2 - v1).norm();
    assert(length != 0.0f);
    const size_t pid1=graph.get_patchId(seam_edge.f1),
               pid2=graph.get_patchId(seam_edge.f2);
    std::vector<EdgeProjectionInfo> edge_projection_infos(2);
    edge_projection_infos[0].texture_patch_id=pid1;
    edge_projection_infos[1].texture_patch_id=pid2;
    for(auto &vpi:vertex_projection_infos[seam_edge.v1]){
      if (vpi.texture_patch_id==pid1){
        edge_projection_infos[0].p1=vpi.projection;
      }
      if (vpi.texture_patch_id==pid2){
        edge_projection_infos[1].p1=vpi.projection;
      }
    }
    for(auto &vpi:vertex_projection_infos[seam_edge.v2]){
      if (vpi.texture_patch_id==pid1){
        edge_projection_infos[0].p2=vpi.projection;
      }
      if (vpi.texture_patch_id==pid2){
        edge_projection_infos[1].p2=vpi.projection;
      }
    }

    std::size_t num_samples = 0;
    for (EdgeProjectionInfo const &halfEPI : edge_projection_infos) {
      TexturePatch::Ptr patch = texture_patches[halfEPI.texture_patch_id];
      const int patch_label = patch->get_label();
      assert (patch_label == label1 || patch_label == label2);
      if (patch_label == label1)
        color1_accum.add(sample_edge(patch, halfEPI.p1, halfEPI.p2), length);
      if (patch_label == label2)
        color2_accum.add(sample_edge(patch, halfEPI.p1, halfEPI.p2), length);
      num_samples++;
    }
    //断言:一条seam edge,会且只会被拆成两个半边
    assert(num_samples == 2);
  }
  math::Vec3f color1 = color1_accum.normalized();
  math::Vec3f color2 = color2_accum.normalized();

  /* The order is essential. */
  math::Vec3f difference = color2 - color1; ///大标签-小标签

  assert(!std::isnan(difference[0]));
  assert(!std::isnan(difference[1]));
  assert(!std::isnan(difference[2]));

  return difference;
}

struct LabelPairCompare{
bool operator() (const std::pair<int,int> &l,
                      const std::pair<int,int> &r) const{
  if (l.first<r.first){
    return true;
  } else if (l.first==r.first){
    return l.second<=r.second;
  }else{
    return false;
  }
}
};

void global_seam_leveling_fix(UniGraph const &graph,
                          mve::TriangleMesh::ConstPtr mesh,
                          mve::MeshInfo const &mesh_info,
                          VertexProjectionInfos const &global_vpi,
                          std::vector<TexturePatch::Ptr> *texture_patches) {
  using namespace std;
  //using tbb::parallel_for;using tbb::blocked_range;
  util::WallTimer timer;
  mve::TriangleMesh::VertexList const &vertices = mesh->get_vertices();
  mve::TriangleMesh::FaceList const &faces = mesh->get_faces();
  size_t const VN = vertices.size();
  cout << "\tCreate matrices for optimization... " << endl;
  /* <vid-label> --> g_row 的映射表:即顶点标签组合 到
  g向量行号的映射,用于构造A和gamma.
   *      V0L0   --> 0
   *      V0L1   --> 1
   *      V0L2   --> 2
   *      V0L4   --> 3
   *      V1L0   --> 4
   *      V1L3   --> 5
   *      V1L4   --> 6
   *      V2L1   --> 7
   *      V2L2   --> 8
   *      ...    ...
   * 对应的patch划分示例:(图1)
  V9.....
  . .     ......
  .  ..          .....
  .    ..             ....
   .     ...               .....
   .       ..      P3           . .V3.. . . . . .. . .. .V4.
    .        ..                 .  .                   ..    .
    .           .             ..   .                 ..        .
     .           ..          .     .                .            .
     .              ..     .       .     P0       .                .
      .                .V1.        .             .                  .
      .     P3         .. .   P0   .           .                     .V5
       .               .   .       .         .       P1           ..  .
       .              ..    .      .        .                ...    . .
       ..            ..      .     .      .             ...       ..  .
        .           ..        ..   .     .            ..        ...   .
        ..          .           .  .   .          ...          ..     .
         .         .   P4        . .  .     ...               ..      .
         .        ..            . .V0.  .                    ..       .
         ..      ..         . .    .      .          P2     .         .
          .     ..     .  .        .        . .            .          .
          ..   ..  . .             .             .        .           .
           .  .. .       P4        .     P2         .  . .            .
           .....                   .                    ..     P1     .
            V8.....................V7....................V2           .
                                    ...                   ...         ..
                                       ....                 ..         .
                                           ...               ..        .
                                              . ..    P2      .        .
                                                 ....          ..      .
                                                    ....        ..     .
                                                       ...       .     .
                                                          ...     .    .
                                                             ...   ..  .
                                                                ... .. .
                                                                  .... .
                                                                      V6
   Pi代表patch id,patch与标签是n:1关系:
   一个patch只能有一个标签,但一个标签可以标记给多个patch.
   上图假设每个patch与label一一对应.
   */
  vector<map<size_t, size_t>> vertlabel2row;
  vertlabel2row.resize(VN);
  //记录每个顶点周围的所有标签号，不含标签0
  vector<vector<size_t>> labels;
  labels.resize(VN);

  /*Assign each vertex for each label a new index(row) within the solution X.*/
  atomic<size_t> x_row(0);
  ({for (size_t i = 0; i < VN; ++i) {
    set<size_t> label_set;
    const vector<size_t> &adj_faces = mesh_info[i].faces;
    set<size_t>::iterator it = label_set.begin();
    for (size_t j = 0; j < adj_faces.size(); ++j) {
      size_t label = graph.get_label(adj_faces[j]);
      label_set.insert(it, label);//默认升序
    }
    //标签集升序,使得下面A和gamma矩阵的列,X和b向量的行都按照这种ViLj,i主键j次键升序排序
    for (it = label_set.begin(); it != label_set.end(); ++it) {
      size_t label = *it;
      if (label == 0)
        continue;
      vertlabel2row[i][label] = x_row;
      labels[i].push_back(label);
      ++x_row;
    }
  }
  });

  size_t x_rows = x_row;
  assert(x_rows < (size_t)(numeric_limits<int>::max()));
  cout << "\t\tfill vertlable2row:" << timer.get_elapsed_sec() << "s" << endl;
  timer.reset();

  /* 生成gamma 矩阵  Fill the Tikhonov matrix Gamma(regularization
   * constraints):
   * 行\列
   *  号\号  V0L0 V0L1 V0L2 V0L4 V1L0 V1L3 V1L4 V2L1 V2L2 .... x_rows
   *   0      1                  -1
   *   1      1                                           .
   *   2      1                                             .
   *   3          1                                          .
   *   4          1                                            .
   *   5                1                                      .
   *   6                1                             -1
   *   7                1                                        .
   *   8                    1              -1
   *   9                    1                                    .
   *  10                    1                                     .
   *  11                         1                         .(v1L0-v0L0重复,不加)
   *  12                               1                    .
   *   .
   *   .
   * Gamma_rows
   * Note:在往gamma矩阵加入每一行(ViLm - VjLm)时，必须Vi<Vj，避免重复.
   */
  vector<future<void>> Gamma_Ab_future;
  typedef Eigen::Triplet<float, int> GammaTriplet;
  vector<GammaTriplet> coefs_Gamma;
  coefs_Gamma.reserve(2 * VN);
  SpMat Gamma;
  Gamma_Ab_future.push_back(async([&coefs_Gamma,&Gamma,&faces,&x_rows,
    &VN,&global_vpi,&texture_patches,&vertlabel2row]()
  {
    util::WallTimer timer;
    constexpr float lambda = 0.05f;
    size_t Gamma_rows=0;
    size_t Gamma_row=0;
    for (size_t i = 0; i < VN; ++i) {
      // 遍历Vi的所有VPI,在相同patch内找它的邻接点,找到之后用对
      // 应patch的标签确定这对邻接点的g之差在gamma矩阵中的位置.
      const auto & vpis=global_vpi[i];
      for(size_t m=0; m<vpis.size(); m++){
        auto &vpi = vpis[m];
        const auto label=texture_patches->at(vpi.texture_patch_id)->get_label();
        if (label<=0) continue;
        for(auto & fid:vpi.faces){
          for(size_t r=0; r<3; r++){
            const auto adj_vid=faces[fid*3+r];
            if (i>=adj_vid) continue;
            GammaTriplet t1(Gamma_row, vertlabel2row[i][label], lambda);
            GammaTriplet t2(Gamma_row, vertlabel2row[adj_vid][label], -lambda);
            coefs_Gamma.push_back(t1);
            coefs_Gamma.push_back(t2);
            Gamma_row++;
          }
        }
      }
    }
    Gamma_rows = Gamma_row;
    assert(Gamma_rows < (size_t)(numeric_limits<int>::max()));
    Gamma.resize(Gamma_rows, x_rows);
    Gamma.setFromTriplets(coefs_Gamma.begin(), coefs_Gamma.end());
    coefs_Gamma.clear();
    cout << "\t\t𝛾:"<<Gamma_rows<<"x"<<x_rows<<"," << timer.get_elapsed_sec() << "s" << endl;
  }
  ));

  /** 生成矩阵A和向量b.*/
  vector<Eigen::Triplet<float, int>> coefs_A;
  SpMat A;
  vector<math::Vec3f> coefs_b;
  size_t A_rows=0;
  Gamma_Ab_future.push_back(async([&coefs_A,&coefs_b,&VN,&A,&x_rows,&A_rows,
    &vertices,&vertlabel2row,&mesh_info,&graph,&global_vpi,&texture_patches]()
  {
    util::WallTimer timer;
    size_t A_row = 0;
    LabelPairCompare cmp;
    for (size_t i = 0; i < VN; ++i) {
      /*
       * 遍历顶点i的每个邻接点,它们形成一条边,可以找到左右两个面,进
       * 而从UniGraph得到它们的标签,若标签不同,则发现了一条seam edge
       */
      map<pair<int,int>,vector<SeamEdge>,LabelPairCompare> seam_edges(cmp);
      const auto &adj_verts=mesh_info[i].verts;
      for(size_t j=0; j<adj_verts.size(); j++){
        vector<size_t> edge_faces;
        const auto adj_vid=adj_verts[j];
        if ((vertices[adj_vid] - vertices[i]).norm() == 0.0f) {
          cout << "warn:zero-length edge.skip."<<i<<","<<adj_vid<< endl;
          continue;
        }
        mesh_info.get_faces_for_edge(i,adj_vid,&edge_faces);
        if (edge_faces.size()>2){
          cerr<<"AMat:edge(v"<<i<<",v"<<adj_vid<<") isn't simplex!"<<endl;
          continue;
        } else if(edge_faces.size()<=1){
          //cout<<"AMat:edge(v"<<i<<",v"<<adj_vid<<") is border!"<<endl;
          continue;
        }
        int label1=(int)graph.get_label(edge_faces[0]);
        int label2=(int)graph.get_label(edge_faces[1]);
        if (label1==0||label2==0){
          //cout<<"AMat:edge(v"<<i<<",v"<<adj_vid<<") at zero label!"<<endl;
          continue;
        }
        if (label1==label2)continue;
        if (label1>label2){swap(label1,label2);}
        seam_edges[make_pair(label1,label2)].emplace_back(i,adj_vid,edge_faces[0],edge_faces[1]);
      }
      //遍历seam_edges,构造A矩阵和b向量的元素,二者的行号意义要保持一致
      auto it=seam_edges.begin();
      for(;it!=seam_edges.end();it++){
        const auto label1=it->first.first, label2=it->first.second;
        assert(0<label1&&label1<label2);
        //A矩阵的某一行<1,-1>，与g向量<gViLabel1,-ViLabel2>
        Eigen::Triplet<float, int> t1(A_row, vertlabel2row[i][label1], 1.0f);
        Eigen::Triplet<float, int> t2(A_row, vertlabel2row[i][label2], -1.0f);
        coefs_A.push_back(t1);
        coefs_A.push_back(t2);
        //b向量的某一行:
        coefs_b.push_back(calculate_difference(global_vpi,
                vertices,graph,*texture_patches,it->second,label1,label2));
        ++A_row;
      }
    }
    A_rows = A_row;
    assert(A_rows < (size_t)(numeric_limits<int>::max()));
    A.resize(A_rows, x_rows);
    A.setFromTriplets(coefs_A.begin(), coefs_A.end());
    coefs_A.clear();
    cout << "\t\tA:"<<A_rows<<"x"<<x_rows<<"," << timer.get_elapsed_sec() << "s" << endl;
  }
  ));
  for(auto &f:Gamma_Ab_future){f.get();}
  cout<<"\t\tGamma_Ab:"<<timer.get_elapsed_sec()<<"s"<<endl;
  timer.reset();
  SpMat Lhs = A.transpose() * A + Gamma.transpose() * Gamma;
  /* Only keep lower triangle(conjugate gradient) only uses the lower) */
  Lhs.prune([](const int &row, const int &col, const float &value) -> bool {
    return col <= row && value != 0.0f;
  });
  Gamma.resize(0,0);Gamma.data().squeeze();
  cout << "\t\tLhs:" << timer.get_elapsed_sec() << "s,"
        << Lhs.rows() << " x " << Lhs.cols()<< endl;
  timer.reset();

  vector<map<size_t, math::Vec3f>> adjust_values(VN);
  vector<Eigen::VectorXf> X(3);//分别记录三个通道的解
  cout << "\tCalculating adjustments:" << endl;
  //todo:尝试把b向量改成亮度通道.前面计算b_coefficients时是RGB线性运算,可以在这直接转为亮度通道
  #ifndef SERIAL_LEVELING
  parallel_for(blocked_range<size_t>(0,3),[&Lhs,&A,&coefs_b,&x_rows,&labels,&vertlabel2row,&adjust_values,&VN,&X](const blocked_range<size_t> &r){for(size_t channel=r.begin(); channel!=r.end(); channel++){
  #else
  ({for (size_t channel = 0; channel < 3; ++channel) {
  #endif
    Eigen::ConjugateGradient<SpMat, Eigen::Lower> cg;
    cg.setMaxIterations(10000);
    cg.setTolerance(0.0001);
    cg.compute(Lhs);
    /* Prepare right hand side. */
    Eigen::VectorXf b(A.rows());
    for (size_t i = 0; i < coefs_b.size(); ++i) {
      b[i] = coefs_b[i][channel];
    }
    Eigen::VectorXf Rhs = SpMat(A.transpose()) * b;
    /* Solve for x. */
    X[channel].resize(x_rows);
    X[channel] = cg.solve(Rhs);
    //cout << "\t\tcg:" << timer.get_elapsed_sec() << "s" << endl;
    //timer.reset();
    /*float *xvalue = x.data();
    ofstream xout("/tmp/zm3d/x0.txt");
    for (int l = 0; l < x_rows; ++l) {
      xout << xvalue[l] << endl;
    }
    xout.close();*/

    /**Subtract mean because system is
     * underconstrained and we seek the solution with minimal adjustments. */
    X[channel] = X[channel].array() - X[channel].mean();
    /*cout << "\t\tColor channel " << channel << ": CG took " << cg.iterations()
         << " iterations. Residual is " << cg.error() << endl;*/
  }
  });

  for (size_t i = 0; i < VN; ++i) {
    for (size_t j = 0; j < labels[i].size(); ++j) {
      size_t label = labels[i][j];
      adjust_values[i][label][0] = X[0][vertlabel2row[i][label]];
      adjust_values[i][label][1] = X[1][vertlabel2row[i][label]];
      adjust_values[i][label][2] = X[2][vertlabel2row[i][label]];
    }
  }
  //==clear==
  A.resize(0,0);A.data().squeeze();Lhs.resize(0,0);Lhs.data().squeeze();
  coefs_b.clear();
  X.clear();
  cout << "\t\tcg total:" << timer.get_elapsed_sec() << " s" << endl;
  timer.reset();

  mve::TriangleMesh::FaceList const &mesh_faces = mesh->get_faces();
  const size_t PN=texture_patches->size();
  ProgressCounter texture_patch_counter(
            "\tAdjusting texture patches", texture_patches->size());
  #ifndef SERIAL_LEVELING
  parallel_for(blocked_range<size_t>(0,PN),[&texture_patches,&adjust_values,&mesh_faces](const blocked_range<size_t> &r){ for(auto i=r.begin(); i!=r.end(); i++){
  #else
  ({for (size_t i = 0; i < texture_patches->size(); ++i) {
  #endif
    texture_patch_counter.progress<SIMPLE>();
    util::WallTimer timer_colors;
    TexturePatch::Ptr texture_patch = texture_patches->at(i);
    const int label = texture_patch->get_label();
    vector<size_t> const &faces = texture_patch->get_faces();
    vector<math::Vec3f> patch_adjust_values(faces.size() * 3, math::Vec3f(0.f));
    /** Only adjust texture_patches originating form input images. */
    if (label == 0) {
      texture_patch->adjust_colors(patch_adjust_values);
      texture_patch_counter.inc();
      continue;
    };
    for (size_t j = 0; j < faces.size(); ++j) {
      for (size_t k = 0; k < 3; ++k) {
        size_t face_pos = faces[j] * 3 + k;
        size_t vertex = mesh_faces[face_pos];
        patch_adjust_values[j * 3 + k] = adjust_values[vertex].find(label)->second;
      }
    }
    texture_patch->adjust_colors(patch_adjust_values);
    texture_patch_counter.inc();
    cout << "\t\tadjust_colors L"<<label<<"p"<<i<<":"
         <<timer_colors.get_elapsed_sec() << "s" << endl;
  }
  });
  cout << "\t\tadjust colors total:" << timer.get_elapsed_sec() << "s" << endl;
}

void debug_output_eigen_spmat(SpMat &Lhs,EigenSpMatTriangle triangle,
                              const std::string&file){
  //std::vector<int> rowIndice;
  //std::vector<float> value;
  //std::vector<long> columnStart;
  int maxrow = 0;
  //SparseMatrix<Scalar,Options,IndexType>
  // it an alias of the third template parameter IndexType.
  int k = 0;
  std::ofstream fout(file);
  int col = 0;
  for (int j = 0; j < Lhs.outerSize(); ++j) {
    int previousStart = Lhs.outerIndexPtr()[j]; // 0
    //columnStart.push_back((long)k);
    fout << col << "列:";
    int end = Lhs.outerIndexPtr()[j + 1];
    for (int i = previousStart; i < end; ++i) {
      float cofficient = Lhs.valuePtr()[i];
      int row = Lhs.innerIndexPtr()[i];
      //下三角nnz
      if ((
            (triangle==EigenSpMatTriangle_Lower&&j<=row)||
            (triangle==EigenSpMatTriangle_Upper&&j>=row)||
            triangle==EigenSpMatTriangle_None
          ) && (cofficient != 0.0f)) {
        fout << row<<":"<<cofficient << " "; //将变量的值写入文件
        //value.push_back(cofficient);
        //rowIndice.push_back(row);
        ++k;
      }
    }
    fout << std::endl;
    col++;
  }
  //columnStart.push_back((long)k);
  fout.close();
}

TEX_NAMESPACE_END
