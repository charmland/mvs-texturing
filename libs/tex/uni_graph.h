/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_UNIGRAPH_HEADER
#define TEX_UNIGRAPH_HEADER

#include <algorithm>
#include <cassert>
#include <vector>
#include "defines.h"

TEX_NAMESPACE_BEGIN
/**
 * Implementation of a unidirectional graph with fixed amount of nodes using
 * adjacency lists.
 *
 * 单向邻接图.
 *
 */
class UniGraph {
private:
  /**
   * 每个三角面的邻接面.
   * 此数组的大小为mesh中三角面的总数目,亦即此图中的节点数目.
                     +----->F0
                     |
                     +------->F3
                     |
             +---->F1+----->F8
             |
      F0 +---+        +---->F0
             +----->F2|---->F4
             |        +------->F5
             |
             +---->F7
    F0与(F1,F2,F7)邻接，F1与(F0,F3,F8)邻接。F0的邻接表中包含<F0,F1>，而F1的邻接
    列表中包含<F1,F0>，这可以看作是两个半边! 例如:
               X
             X   X
           X  . | X
          X  /| |   X
         X    | |     X
        X     | |       X
      X       | |        X
     X        | |         X
    XX        | |        X
      X       | |       X
       XX     | |      X
         X    | |    X
          X   | |   X
           XX | |/ X
             X| . X
              X  X
               X

   *且按照adj_lists的构造方式，每个面的邻接面都是按照边的逆时针顺序安排.
   */
  std::vector<std::vector<std::size_t>> adj_lists;
  ///< 记录每个面分配到的标签
  std::vector<std::size_t> labels;
  /**
   * mesh中出去边缘边以外所有半边的总数，并不等于adj_lists的大小，而是等于
   * adj_lists的二级列表长度的总和.
   */
  std::size_t edges;
  /**
   * 每个面的texture patch id.
   */
  std::vector<unsigned short> face2patchid;
public:
  /**
   * Creates a unidirectional graph without edges.
   * @param nodes number of nodes.
   */
  UniGraph(std::size_t nodes);

  /**
   * Adds an edge between the nodes with indices n1 and n2.
   * If the edge exists nothing happens.
   * @warning asserts that the indices are valid.
   */
  void add_edge(std::size_t n1, std::size_t n2);

  /**
   * Removes the edge between the nodes with indices n1 and n2.
   * If the edge does not exist nothing happens.
   * @warning asserts that the indices are valid.
   */
  void remove_edge(std::size_t n1, std::size_t n2);

  /**
   * Returns true if an edge between the nodes with indices n1 and n2 exists.
   * @warning asserts that the indices are valid.
   */
  bool has_edge(std::size_t n1, std::size_t n2) const;

  /** Returns the number of edges. 此单向邻接图中边数,也是mesh非边界边上的半边总数目*/
  std::size_t num_edges() const;

  /** Returns the number of nodes.例如三角面数量 */
  std::size_t num_nodes() const;

  /**
   * Sets the label of node with index n to label.
   * @warning asserts that the index is valid.
   */
  void set_label(std::size_t n, std::size_t label);

  /**
   * Returns the label of node with index n.
   * @warning asserts that the index is valid.
   */
  std::size_t get_label(std::size_t n) const;

  /**
   * Fills given vector with all subgraphs of the given label.
   * A subgraph is a vector containing all indices of connected nodes with the
   * same label.
   * 在label指定的视图中找出所有联通子图.
   */
  void get_subgraphs(std::size_t label,
                     std::vector<std::vector<std::size_t>> *subgraphs) const;

  std::vector<std::size_t> const &get_adj_nodes(std::size_t node) const;

  ///获取face->patchId的映射
  std::vector<unsigned short> &get_face2patchid();

  int get_patchId(std::size_t nodeId) const ;
};

/**
 * 向邻接图中添加两个半边,分别是<n1,n2>和<n2,n1>.
 * 此函数内部会判断这两条半边是否已存在，若存在则什么也不做.
 * @param n1
 * @param n2
 */
inline void UniGraph::add_edge(std::size_t n1, std::size_t n2) {
  assert(n1 < num_nodes() && n2 < num_nodes());
  if (!has_edge(n1, n2)) {
    adj_lists[n1].push_back(n2);
    adj_lists[n2].push_back(n1);
    ++edges;
  }
}

inline void delete_element(std::vector<std::size_t> *vec, std::size_t element) {
  vec->erase(std::remove(vec->begin(), vec->end(), element), vec->end());
}

inline void UniGraph::remove_edge(std::size_t n1, std::size_t n2) {
  assert(n1 < num_nodes() && n2 < num_nodes());
  if (has_edge(n1, n2)) {
    delete_element(&adj_lists[n1], n2);
    delete_element(&adj_lists[n2], n1);
    --edges;
  }
}

inline bool UniGraph::has_edge(std::size_t n1, std::size_t n2) const {
  assert(n1 < num_nodes() && n2 < num_nodes());
  std::vector<std::size_t> const &adj_list = adj_lists[n1];
  return std::find(adj_list.begin(), adj_list.end(), n2) != adj_list.end();
}

inline std::size_t UniGraph::num_edges() const { return edges; }

inline std::size_t UniGraph::num_nodes() const { return adj_lists.size(); }

inline void UniGraph::set_label(std::size_t n, std::size_t label) {
  assert(n < num_nodes());
  labels[n] = label;
}

inline std::size_t UniGraph::get_label(std::size_t n) const {
  assert(n < num_nodes());
  return labels[n];
}

/**
 * 查询节点id所属的texture patch id.
 * @note 节点到patch id的映射表，在generate_texture_patch()之后才能构建好,在这之前
 * 调用此方法会返回-1.
 * @param nodeId 节点id,即face id.
 * @return 返回值>=0 代表是texture patch id,可用于TexturePathes索引;返回值<0表明出错.
 */
inline int UniGraph::get_patchId(std::size_t nodeId) const {
  assert(nodeId<num_nodes());
  if (face2patchid.size()<=0){return -1;}
  return face2patchid[nodeId];
}

/**
 * 获取node所代表的三角面的所有邻接面
 * @param node 单向邻接图中某个节点，即mesh中某个三角面的编号
 * @return \p node三角面的邻接节点的编号列表
 */
inline std::vector<std::size_t> const &
UniGraph::get_adj_nodes(std::size_t node) const {
  assert(node < num_nodes());
  return adj_lists[node];
}

inline std::vector<unsigned short> &
UniGraph::get_face2patchid() {
  return this->face2patchid;
}

TEX_NAMESPACE_END

#endif /* TEX_UNIGRAPH_HEADER */
