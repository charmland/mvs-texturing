// facial_feature_prior.cpp
// Created by chenglong on 2018/8/17.
// Copyright © 2018 www.zhimei.ai All rights reserved.
// 此模块用于集成人脸特征信息到纹理映射算法中,目前包括:
// 1.在MRF划分patch时，对五官区域的三角面投影到正脸视图的数据项权重进行提升(代价降低)
// 2.在所有视图中找出正脸视图
#include "defines.h"
#include "texture_view.h"
#include "texturing.h"
#include "rect.h"
#include "PMP/common/FaceLandmark.hpp"

TEX_NAMESPACE_BEGIN
/**
 * 根据texture view中的yaw角，找出正脸视图
 * @param views
 * @return 返回正脸视图的索引编号
 */
std::size_t get_frontal_face_view_index(const TextureViews &views) {
  std::size_t frontal_idx = 0;
  float min_abs_yaw = std::numeric_limits<float>::max();
  for (int i = 0; i < views.size(); i++) {
    auto &view = views[i];
    float abs_yaw = std::abs(view.get_yaw());
    if (abs_yaw < min_abs_yaw) {
      min_abs_yaw = abs_yaw;
      frontal_idx = i;
    }
  }
  return frontal_idx;
}

/**
 * 在正脸视图中找出眉毛+眼睛，左右各构成一个Rect；找出嘴巴的特征点，构成一个Rect.
 * @todo 目前直接简单地使用五官矩形框来选取一批三角形,对这批三角形进行投影提权.
 * @param frontal_view 正脸视图
 * @return 返回多个五官区域的Rect,每个Rect都以像素为单位
 */
FacialFeatureRectPtr
construct_facial_feature_rects(const TextureView &frontal_view) {
  using namespace FaceLandmark;
  using Rect = Rect<float>;
  constexpr float FLOAT_MIN = std::numeric_limits<float>::lowest(),
                  FLOAT_MAX = std::numeric_limits<float>::max();
  FacialFeatureRectPtr rects(new FacialFeatureRect(4,Rect(FLOAT_MAX,
                        FLOAT_MAX, FLOAT_MIN, FLOAT_MIN,false)));
  TextureView::Landmark2DArrPtr ldmk2ds = frontal_view.get_ldmk2darr();
  //眉毛的话，先只考虑四个点，眉角的点不纳入Rect
  // 18~21
  std::vector<math::Vec2f> eye_L(ldmk2ds->begin() + EYEBROW_LEFT_1,
                                 ldmk2ds->begin() + EYEBROW_LEFT_5+1);
  // 36~41
  eye_L.insert(eye_L.end(), ldmk2ds->begin() + EYE_LEFT_1,
               ldmk2ds->begin() + EYE_LEFT_6+1);
  // 22~25
  std::vector<math::Vec2f> eye_R(ldmk2ds->begin() + EYEBROW_RIHGT_1,
                                 ldmk2ds->begin() + EYEBROW_RIHGT_5+1);
  // 42~47
  eye_R.insert(eye_R.end(), ldmk2ds->begin() + EYE_RIGHT_1,
               ldmk2ds->begin() + EYE_RIGHT_6+1);
  //嘴巴的特征点:48~59
  std::vector<math::Vec2f> mouth(ldmk2ds->begin() + MOUTH_EXTERNAL_1,
                                 ldmk2ds->begin() + MOUTH_EXTERNAL_12+1);
  //鼻子的特征点
  std::vector<math::Vec2f> nose(ldmk2ds->begin()+NOSE_1,
                                ldmk2ds->begin()+NOSE_9+1);
  Rect &rect_eye_L = rects->at(0), &rect_eye_R = rects->at(1),
       &rect_mouth = rects->at(2), &rect_nose=rects->at(3);
  const int w = frontal_view.get_width(), h = frontal_view.get_height();
  for (int i = 0; i < eye_L.size(); i++) {
    rect_eye_L.min_x = std::min(rect_eye_L.min_x, eye_L[i][0] * w);
    rect_eye_L.min_y = std::min(rect_eye_L.min_y, eye_L[i][1] * h);
    rect_eye_L.max_x = std::max(rect_eye_L.max_x, eye_L[i][0] * w);
    rect_eye_L.max_y = std::max(rect_eye_L.max_y, eye_L[i][1] * h);
    rect_eye_R.min_x = std::min(rect_eye_R.min_x, eye_R[i][0] * w);
    rect_eye_R.min_y = std::min(rect_eye_R.min_y, eye_R[i][1] * h);
    rect_eye_R.max_x = std::max(rect_eye_R.max_x, eye_R[i][0] * w);
    rect_eye_R.max_y = std::max(rect_eye_R.max_y, eye_R[i][1] * h);
  }
  for (int j = 0; j < mouth.size(); j++) {
    rect_mouth.min_x = std::min(rect_mouth.min_x, mouth[j][0] * w);
    rect_mouth.min_y = std::min(rect_mouth.min_y, mouth[j][1] * h);
    rect_mouth.max_x = std::max(rect_mouth.max_x, mouth[j][0] * w);
    rect_mouth.max_y = std::max(rect_mouth.max_y, mouth[j][1] * h);
  }
  for(int k=0; k<nose.size(); k++){
    rect_nose.min_x = std::min(rect_nose.min_x,nose[k][0]*w);
    rect_nose.min_y = std::min(rect_nose.min_y,nose[k][1]*h);
    rect_nose.max_x = std::max(rect_nose.max_x,nose[k][0]*w);
    rect_nose.max_y = std::max(rect_nose.max_y,nose[k][1]*h);
  }
  //放大:眼睛和嘴巴底线往下放一点,鼻子两端点
  rect_eye_L.max_y = std::min(h-1.f,rect_eye_L.max_y*1.05f);
  rect_eye_R.max_y = std::min(h-1.f,rect_eye_R.max_y*1.05f);
  rect_mouth.max_y = std::min(h-1.f,rect_mouth.max_y*1.05f);
  //鼻子Rect放宽,大概率会纳入鼻翼和鼻孔,通常不会有问题.
  //warn:但正面视图中若过分低头,则鼻孔不可见,进而可能鼻翼和鼻孔被分割,即分别来自不同的视图
  rect_nose.min_x = std::max(0.f,rect_nose.min_x*0.95f);
  rect_nose.max_x = std::min(w-1.f,rect_nose.max_x*1.05f);
  #ifdef FACIAL_PRIOR_STAT
  for(int k=0; k<rects->size(); k++){
    std::cout<<"Rect:"<<rects->at(k).to_string()<<std::endl;
  }
  #endif
  return rects;
}
TEX_NAMESPACE_END
