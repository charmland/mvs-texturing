/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_PROGRESSCOUNTER_HEADER
#define TEX_PROGRESSCOUNTER_HEADER

#include <atomic>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <tbb/mutex.h>
#include "util/timer.h"
#include "defines.h"

TEX_NAMESPACE_BEGIN


enum ProgressCounterStyle { ETA, SIMPLE };
//static const std::string clear = "\r" + std::string(80, ' ') + "\r";
static const std::string clear="\r";
/**
 * 进度指示器.
 *
 * 用于将某任务的当前执行进度打印出来.
 *
 * 在PC端调试时可以开启.在移动端进度指示并无太大意义,先关闭.
 */
class ProgressCounter {
private:
  std::ofstream tty;
  util::WallTimer timer;
  std::string task;
  std::size_t max;
  std::atomic_size_t count;
  typedef tbb ::mutex IOMutex;
  IOMutex incMutex;
  IOMutex progressMutex;
public:
  ProgressCounter(std::string const &_task, std::size_t max);
  template <ProgressCounterStyle T>
  void progress();
  void inc();
  void reset(std::string const &_task);
};

inline ProgressCounter::ProgressCounter(std::string const &_task,
                                        std::size_t _max)
    : tty("/dev/tty", std::ios_base::out), timer(), task(_task), max(_max),
      count(0) {}

inline void ProgressCounter::inc() {
#ifdef ZMTEX_PC_DEBUG
  std::size_t tmp;
  tmp = ++count;

  if (tmp == max) {
    std::stringstream ss;
    ss << clear << task << " 100%... done. (Took " << timer.get_elapsed_sec()
       << "s)";
//#pragma omp critical(progress_counter_inc)
    {
      IOMutex::scoped_lock lock(incMutex);
      std::cout << ss.rdbuf() << std::endl;
    }
  }
#endif
}

inline void ProgressCounter::reset(std::string const &_task) {
  timer.reset();
  count = 0;
  task = _task;
}

template <ProgressCounterStyle T>
void ProgressCounter::progress() {
  #ifdef ZMTEX_PC_DEBUG
  //max超过100则跳步打印;低于100就每一步都打印
  if ((max > 100 && count % (max / 100) == 0) || max <= 100) {

    float percent = static_cast<float>(count) / max;
    int ipercent = std::floor(percent * 100.0f + 0.5f);//四舍五入

    std::stringstream ss;
    ss << clear << task << " " << ipercent << "%...";

    if (T == ETA && ipercent > 3) {
      std::size_t const elapsed = timer.get_elapsed();
      std::size_t eta = (elapsed / percent - elapsed) / 1000;
      ss << " eta ~ " << eta << " s";
    }
//#pragma omp critical(progress_counter_progress)
    {
      IOMutex::scoped_lock lock(progressMutex);
      //tty << ss.rdbuf() << std::flush;
      std::cout << ss.rdbuf() << std::flush;
    }
  }
  #endif
}

TEX_NAMESPACE_END

#endif /* TEX_PROGRESSCOUNTER_HEADER */
