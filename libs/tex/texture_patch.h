/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_TEXTUREPATCH_HEADER
#define TEX_TEXTUREPATCH_HEADER

#include <vector>

#include <math/vector.h>
#include <mve/mesh.h>

#include "poisson_blending.h"
#include "tri.h"

int const texture_patch_border = 1;

TEX_NAMESPACE_BEGIN

/**
 * Class representing a texture patch.
 * Contains additionaly to the rectangular part of the TextureView
 * the faces which it textures and their relative texture coordinates.
 */
class TexturePatch {
public:
  typedef std::shared_ptr<TexturePatch> Ptr;
  typedef std::shared_ptr<const TexturePatch> ConstPtr;
  typedef std::vector<std::size_t> Faces;
  typedef std::vector<math::Vec2f> Texcoords;

private:
  int label;///<分配的标签id，即视图id
  Faces faces;///<组成此patch的三角面集合
  Texcoords texcoords;///<纹理坐标集合(按照faces中三角面的顶点顺序排开)
  /**
   * 图像内容,三通道浮点图[0,1].
   * @note 这个图像最初是在generate_texture_patches()中由8UC3转换为32FC3得到的,
   * 内存占用量会翻三倍
   */
  mve::FloatImage::Ptr image;
  mve::ByteImage::Ptr validity_mask;
  /**
   * 用于泊松融合的mask:255-条带内部颜色,128-条带内边界,64-条带外边界,0-被条带围住的.
   * 内边界和外边界均不属于条带,内边界属于被条带围住的图像内部,外边界属于其他周边patch.
             ..................................
            .                                 .
           .       *********************      .
          .       *                    *      .
         .       *                     *      .
        .       *                      *      .
       .       *                       *      .
      .       *                        *      .
     .       *                         *      .
    .       *                          *      .
   .       *****************************      .
  .                                           .
 ..............................................

   * 
   */
  mve::ByteImage::Ptr blending_mask;

public:
  static const uint8_t BlendMaskValue0,
                      BlendMaskValue64,
                      BlendMaskValue128,
                      BlendMaskValue255,
                      ValidMaskValue255,
                      ValidMaskValue0;
public:
  /** Constructs a texture patch. */
  TexturePatch(int _label,
               std::vector<std::size_t> const &_faces,
               std::vector<math::Vec2f> const &_texcoords,
               mve::FloatImage::Ptr _image);
  //拷贝一份，构造一个独立部分
  TexturePatch(TexturePatch const &texture_patch);
  //拷贝一份,构造一个独立副本
  static TexturePatch::Ptr create(TexturePatch::ConstPtr texture_patch);
  //由label/faces/texcoords/image重新构造一个TexturePatch对象
  static TexturePatch::Ptr create(int label,
                                  std::vector<std::size_t> const &faces,
                                  std::vector<math::Vec2f> const &texcoords,
                                  mve::FloatImage::Ptr image);

  TexturePatch::Ptr duplicate();

  /** Adjust the image colors and update validity mask. */
  void adjust_colors(std::vector<math::Vec3f> const &adjust_values);

  math::Vec3f get_pixel_value(math::Vec2f pixel) const;
  void set_pixel_value(math::Vec2i pixel, math::Vec3f color);

  bool valid_pixel(math::Vec2i pixel) const;
  bool valid_pixel(math::Vec2f pixel) const;

  std::vector<std::size_t> &get_faces();
  std::vector<std::size_t> const &get_faces() const;
  std::vector<math::Vec2f> &get_texcoords();
  std::vector<math::Vec2f> const &get_texcoords() const;

  mve::FloatImage::Ptr get_image();

  mve::FloatImage::ConstPtr get_image() const;
  mve::ByteImage::ConstPtr get_validity_mask() const;
  mve::ByteImage::ConstPtr get_blending_mask() const;

  std::pair<float, float> get_min_max() const;

  void release_blending_mask();
  void prepare_blending_mask(std::size_t strip_width);

  void erode_validity_mask();

  void blend(mve::FloatImage::ConstPtr orig);

  int get_label() const;
  int get_width() const;
  int get_height() const;
  int get_size() const;

public:
  #ifdef ZMTEX_PC_DEBUG
  //仅供单线程环境调试用
  static cv::Mat getBlendMaskColorMap(){
    static cv::Mat custom_colormap_blendmask(256,1,CV_8UC3,cv::Scalar(0,0,0));
    custom_colormap_blendmask.at<cv::Vec3b>(BlendMaskValue0,0)=cv::Vec3b(0,0,0);//black
    custom_colormap_blendmask.at<cv::Vec3b>(BlendMaskValue64,0)=cv::Vec3b(59, 235,255);//yellow
    custom_colormap_blendmask.at<cv::Vec3b>(BlendMaskValue128,0)=cv::Vec3b(0,0,255);//red
    custom_colormap_blendmask.at<cv::Vec3b>(BlendMaskValue255,0)=cv::Vec3b(255,255,255);//white
    return custom_colormap_blendmask;
  }
  //仅供调试使用
  static cv::Mat getColorfulBlendMask(cv::Mat blendmask){
    cv::Mat colorfulMask;
    cv::applyColorMap(blendmask,colorfulMask,getBlendMaskColorMap());
    return colorfulMask;
  }
  #endif
};

inline TexturePatch::Ptr
TexturePatch::create(TexturePatch::ConstPtr texture_patch) {
  return std::make_shared<TexturePatch>(*texture_patch);
}

inline TexturePatch::Ptr
TexturePatch::create(int label,
                     std::vector<std::size_t> const &faces,
                     std::vector<math::Vec2f> const &texcoords,
                     mve::FloatImage::Ptr image) {
  return std::make_shared<TexturePatch>(label, faces, texcoords, image);
}

inline TexturePatch::Ptr TexturePatch::duplicate() {
  return Ptr(new TexturePatch(*this));
}

inline int TexturePatch::get_label() const { return label; }

inline int TexturePatch::get_width() const { return image->width(); }

inline int TexturePatch::get_height() const { return image->height(); }

inline mve::FloatImage::Ptr TexturePatch::get_image() { return image; }

inline mve::FloatImage::ConstPtr TexturePatch::get_image() const {
  return image;
}

inline mve::ByteImage::ConstPtr TexturePatch::get_validity_mask() const {
  return validity_mask;
}

inline mve::ByteImage::ConstPtr TexturePatch::get_blending_mask() const {
  assert(blending_mask != NULL);
  return blending_mask;
}

inline void TexturePatch::release_blending_mask() {
  assert(blending_mask != NULL);
  blending_mask.reset();
}

inline std::vector<math::Vec2f> &TexturePatch::get_texcoords() {
  return texcoords;
}

inline std::vector<std::size_t> &TexturePatch::get_faces() { return faces; }

inline std::vector<math::Vec2f> const &TexturePatch::get_texcoords() const {
  return texcoords;
}

inline std::vector<std::size_t> const &TexturePatch::get_faces() const {
  return faces;
}
//图像中像素数量
inline int TexturePatch::get_size() const {
  return get_width() * get_height();
}

TEX_NAMESPACE_END

#endif /* TEX_TEXTUREPATCH_HEADER */
