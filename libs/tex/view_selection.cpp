/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <util/timer.h>

#include "mapmap/full.h"
#include "texturing.h"
#include "util.h"

TEX_NAMESPACE_BEGIN

void view_selection(DataCosts const &data_costs,
                    UniGraph *graph,
                    Settings const &) {
  using uint_t = unsigned int;
  using cost_t = float;
  constexpr uint_t simd_w = mapmap::sys_max_simd_width<cost_t>();
  using unary_t = mapmap::UnaryTable<cost_t, simd_w>;
  using pairwise_t = mapmap::PairwisePotts<cost_t, simd_w>;

  /* 1.Construct MRF Graph */
  mapmap::Graph<cost_t> mgraph(graph->num_nodes());
  for (std::size_t i = 0; i < graph->num_nodes(); ++i) {
    if (data_costs.col(i).empty())//当前节点在任何视图中都不可见,则返回
      continue;

    std::vector<std::size_t> adj_faces = graph->get_adj_nodes(i);
    for (std::size_t j = 0; j < adj_faces.size(); ++j) {
      std::size_t adj_face = adj_faces[j];
      if (data_costs.col(adj_face).empty())//邻接节点在任何视图中都不可见则忽略它
        continue;

      /* Uni directional.
       * 单向递增? 这是mapMAP的要求？？
       * 原始的graph是记录了所有半边的，物理上是一种类似树形的十字链表，逻辑上是却有环的，
       * 比如mesh中F0与F3相邻接，那么graph中就会在F0这个节点下记录<F0,F3>半边，
       * 且在F3这个节点下面记录<F3,F0>这种半边.
       *
       * 现在换成mgraph，变成单向无环的树，这个树一定是连通的(不考虑mesh中孤岛), 每一条边
       * 反映的都是mesh中的边，只不过现在节点是按编号大小递增串联起来的.
       */
      if (i < adj_face) {
        mgraph.add_edge(i, adj_face, 1.0f);
      }
    }
  }//end for i<num_nodes
  //统计联通子图
  mgraph.update_components();
  /*2.构造每个节点的可行标签集合*/
  mapmap::LabelSet<cost_t, simd_w> label_set(graph->num_nodes(), false);
  for (std::size_t i = 0; i < data_costs.cols(); ++i) {
    DataCosts::Column const &data_costs_for_node = data_costs.col(i);

    std::vector<mapmap::_iv_st<cost_t, simd_w>> labels;
    if (data_costs_for_node.empty()) {
      labels.push_back(0);//0号标签只是个占位标签
    } else {
      labels.resize(data_costs_for_node.size());
      for (std::size_t j = 0; j < data_costs_for_node.size(); ++j) {
        //可行标签id=视图id+1,意味着标签0为无效标签,只为占位
        labels[j] = data_costs_for_node[j].first + 1;
      }
    }
    label_set.set_label_set_for_node(i, labels);
  }

  std::vector<unary_t> unaries;
  unaries.reserve(data_costs.cols());
  pairwise_t pairwise(2.f);
  for (std::size_t i = 0; i < data_costs.cols(); ++i) {
    DataCosts::Column const &data_costs_for_node = data_costs.col(i);

    std::vector<mapmap::_s_t<cost_t, simd_w>> costs;
    if (data_costs_for_node.empty()) {
      //此节点的数据代价都为空,就安插一个数据代价为1.0的?
      costs.push_back(1.0f);
    } else {
      costs.resize(data_costs_for_node.size());
      for (std::size_t j = 0; j < data_costs_for_node.size(); ++j) {
        float cost = data_costs_for_node[j].second;
        costs[j] = cost;
      }
    }

    unaries.emplace_back(i, &label_set);
    unaries.back().set_costs(costs);
  }

  mapmap::StopWhenReturnsDiminish<cost_t, simd_w> terminate(5, 0.01);
  std::vector<mapmap::_iv_st<cost_t, simd_w>> solution;

  auto display = [](const mapmap::luint_t time_ms,
                    const mapmap::_iv_st<cost_t, simd_w> objective) {
    std::cout << "\t\t" << time_ms / 1000 << "\t" << objective << std::endl;
  };

  /* Create mapMAP solver object. */
  mapmap::mapMAP<cost_t, simd_w> solver;
  solver.set_graph(&mgraph);
  solver.set_label_set(&label_set);
  for (std::size_t i = 0; i < graph->num_nodes(); ++i)
    solver.set_unary(i, &unaries[i]);
  solver.set_pairwise(&pairwise);
  solver.set_logging_callback(display);
  solver.set_termination_criterion(&terminate);

  /* Pass configuration arguments (optional) for solve. */
  mapmap::mapMAP_control ctr;
  ctr.use_multilevel = true;
  ctr.use_spanning_tree = true;
  ctr.use_acyclic = true;
  ctr.spanning_tree_multilevel_after_n_iterations = 5;
  ctr.force_acyclic = true;
  ctr.min_acyclic_iterations = 5;
  ctr.relax_acyclic_maximal = true;
  ctr.tree_algorithm = mapmap::LOCK_FREE_TREE_SAMPLER;

  /* Set true for deterministic (but slower) mapMAP execution. */
  ctr.sample_deterministic = false;
  ctr.initial_seed = 548923723;

  std::cout << "\tOptimizing:\n\t\tTime[s]\tEnergy" << std::endl;
  solver.optimize(solution, ctr);

  /* Label 0 is undefined. */
  std::size_t num_labels = data_costs.rows() + 1;
  std::size_t undefined = 0;
  /* Extract resulting labeling from solver. */
  for (std::size_t i = 0; i < graph->num_nodes(); ++i) {
    int label = label_set.label_from_offset(i, solution[i]);
    if (label < 0 || num_labels <= static_cast<std::size_t>(label)) {
      throw std::runtime_error("Incorrect labeling");
    }
    if (label == 0)
      undefined += 1;
    graph->set_label(i, static_cast<std::size_t>(label));
  }
  std::cout << '\t' << undefined << " faces have not been seen" << std::endl;
}

TEX_NAMESPACE_END
