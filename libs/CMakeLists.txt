file (GLOB_RECURSE HEADERS "*.h")
#排除隐藏文件,但中间目录名若带有点号，则也会被忽略
file (GLOB_RECURSE SOURCES "[^.]*.cpp")
#message("SOURCES=${SOURCES}")
file(GLOB_RECURSE PMP_SOURCES "${CMAKE_SOURCE_DIR}/elibs/PMP/*.c??" "${CMAKE_SOURCE_DIR}/elibs/PMP/*.c?")
#message("PMP_SOURCES=${PMP_SOURCES}")

set(LIBRARY tex)
add_library(${LIBRARY} STATIC ${SOURCES} ${PMP_SOURCES})
#set_property(TARGET ${LIBRARY} PROPERTY INTERPROCEDURAL_OPTIMIZATION True)
#add_dependencies(${LIBRARY} ext_mve ext_rayint ext_eigen ext_mapmap)
target_link_libraries(${LIBRARY} tbb -lmve -lmve_util ${JPEG_LIBRARIES} ${PNG_LIBRARIES} ${TIFF_LIBRARIES} ${OpenCV_LIBS})
target_compile_definitions(tex
                PRIVATE DEBUG
                #PATCH_STAT
                #TEXTUREVIEW_STAT
                #FACIAL_PRIOR_STAT
                #PRIVATE ZMTEX_PC_DEBUG  #是否开启pc端调试代码
                SERIAL_LEVELING #是否启用单线程版的global seam leveling
                SERIAL_BLENDING #是否启用单线程版的泊松融合

        )
#install(TARGETS ${LIBRARY} ARCHIVE DESTINATION lib)

